<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>chk_captcha</name>
   <tag></tag>
   <elementGuidId>07c4b812-0244-4ae0-8945-6f1995441ab7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id=&quot;recaptcha-anchor&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Backup/Page_Login/iframe_captcha</value>
      <webElementGuid>a4022c52-28c4-474f-8660-61fbec84f7b6</webElementGuid>
   </webElementProperties>
</WebElementEntity>
