package browser

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.util.KeywordUtil
import org.openqa.selenium.WebElement

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object
import java.io.File;
import java.io.IOException;

public class hla {
	/**
	 * Enter text  element
	 * @param to Katalon test object
	 * @param value
	 */
	@Keyword
	def enter(TestObject to,String value) {
		try {
			WebElement element = WebUI.findWebElement(to, 10)
			KeywordUtil.logInfo("Clear text if exists")
			element.clear()
			KeywordUtil.logInfo("input text into element")
			KeywordUtil.markPassed("Input text in to element successfuylly")
			element.sendKeys(value)
		}
		//				catch (WebElementNotFoundException e) {
		//					KeywordUtil.markFailed("Element not found")
		//				}
		catch (Exception e) {
			KeywordUtil.markFailed("Fail to enter on element")
		}
	}
	@Keyword
	def readFile(String filePath,String retValue) {
		try {
			String data = "";
			File myObj = new File(filePath);
			Scanner myReader = new Scanner(myObj);
			while (myReader.hasNextLine()) {
				data += myReader.nextLine();
				System.out.println(data);
			}
			myReader.close();
			return data;
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}


	private static AmazonS3 s3Client

	static {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIA5FTY7LEKUGX5WAW7", "VmNMj/Q2AApNNy0+iimB9lo1mPQC7rHEvWuKkZ2e")
		s3Client = AmazonS3ClientBuilder.standard()
				.withRegion(Regions.AP_SOUTHEAST_2)
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds))
				.build()
	}

	@Keyword
	static void uploadFileToS3(String bucketName, String fileName, String filePath) {
		File file = new File(filePath)
		s3Client.putObject(new PutObjectRequest(bucketName, fileName, file))
		println("File uploaded successfully to S3 bucket: " + bucketName + " with key: " + fileName)
	}
	@Keyword
	static Boolean doesFileExistOnS3(String bucketName, String fileName) {
		Boolean obj = s3Client.doesObjectExist(bucketName, fileName);
		println("File exists in S3:" +obj.toString());
		return obj;
	}

	@Keyword
	static void waitForFileExistsOnS3(String bucketName, String fileName, int timeOut) {
		long startTime = System.nanoTime();
		Calendar calendar = Calendar.getInstance();
		Boolean isExist = false;
		long count = 0;
		Boolean condition = false;
		//		long startTime = calendar.getTimeInMillis();
		while(count < timeOut* 1000 && isExist ==false){
			Thread.sleep(1000);
			isExist = doesFileExistOnS3(bucketName,fileName);
			long currentTime = calendar.getTimeInMillis();
			long stopTime = System.nanoTime();
			count = (stopTime - startTime)/1000000;
			System.out.println(count);
		}
	}

	@Keyword
	static String readFileOnS3(String bucketName, String fileName) {
		S3Object object = s3Client.getObject(new GetObjectRequest(bucketName, fileName));
		InputStream objectData = new ByteArrayInputStream(object.getObjectContent().readAllBytes());
		// Process the objectData stream.
		//		objectData.close();
		String aa = new String(objectData.readAllBytes());
		return aa;
	}
}
