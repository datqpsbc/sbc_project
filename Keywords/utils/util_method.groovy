package utils

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.util.KeywordUtil

import org.apache.poi.EncryptedDocumentException
import org.apache.poi.hssf.usermodel.HSSFWorkbook

public class util_method {
	private boolean IsExcelFileProtected(String pathFile) {
		try{
			new HSSFWorkbook(new FileInputStream(pathFile));
		}
		catch(EncryptedDocumentException e){
			return true;
		}
		catch(Exception e){
		}
		return false;
	}
	
	@Keyword
	def checkExcelFileProtected(String filePath, boolean expected) {
		boolean isProtected = IsExcelFileProtected(filePath);
		String msg = "";
		if(expected == true) {
			msg = "Expected file has password protected";
		}else {
			msg = "Expected file hasn't password protected";
		}
		if(isProtected == expected) {
			KeywordUtil.markPassed(msg);
		}else {
			KeywordUtil.markFailed(msg + " but actual isn't match with expected");
		}
	}
}
