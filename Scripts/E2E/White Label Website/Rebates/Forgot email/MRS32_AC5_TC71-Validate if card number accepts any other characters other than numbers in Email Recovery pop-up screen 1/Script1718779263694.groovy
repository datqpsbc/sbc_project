import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl(GlobalVariable.wlw_rebates)

WebUI.click(findTestObject('White Label Website/Home_page/link_login'))

WebUI.waitForElementVisible(findTestObject('White Label Website/Home_page/div_loginDialog'), 10)

WebUI.click(findTestObject('White Label Website/Home_page/link_forgotEmail'))

WebUI.waitForElementVisible(findTestObject('White Label Website/Home_page/Forgot email dialog/txtCardNumber'), 0)

WebUI.setText(findTestObject('White Label Website/Home_page/Forgot email dialog/txtCardNumber'), '@#$%^abcd')

WebUI.click(findTestObject('White Label Website/Home_page/Forgot email dialog/btn_Submit'))

var_errorText = WebUI.getText(findTestObject('White Label Website/Home_page/Forgot email dialog/lbl_errorMessage'))

println("$var_errorText")

WebUI.verifyElementText(findTestObject('White Label Website/Home_page/Forgot email dialog/lbl_errorMessage'), 'Please enter a valid account number.')

