<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>e2e</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>dfbbc15b-6f3a-4992-a602-dbf72d16cd65</testSuiteGuid>
   <testCaseLink>
      <guid>5661c9d8-617c-4e8d-b130-a569c8fa1c81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E2E/White Label Website/Rewards/Forgot password/MRS32_AC5_TC81-Validate if Password Reset pop-up screen 1 email field accepts space</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>dd420035-3022-4c26-aa0b-d9a58a2c8340</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E2E/White Label Website/Rebates/Forgot email/MRS32_AC5_TC71-Validate if card number accepts any other characters other than numbers in Email Recovery pop-up screen 1</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5eae1527-919a-4ac0-9357-46f625503437</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/E2E/White Label Website/Rebates/Forgot email/MRS32_AC5_TC68-Validate login process and screen specifications available in Rebates Website-Email Recovery pop-up screen 1</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
